app.constant("Config", {
  "WebUrl": "http://labs.threesquare.in/staging/news_app/api/",
  "AppName" : "NewsClip",
  "AndroidAppUrl" : "",
  "ErrorMessage" : "End of results"
})
// config contact
app.constant("ConfigContact", {
  "EmailId": "vaibhav@threesquare.in",
  "ContactSubject": "Contact"
})
// config admon
//app.constant("ConfigAdmob", {
//  "interstitial": "ca-app-pub-3940256099942544/1033173712",
//  "banner": "ca-app-pub-3940256099942544/6300978111"
//})
// color variations
app.constant("Color", {
  "AppColor": "assertive", //light, stable, positive, calm, balanced, energized, assertive, royal, dark
})
// push notification
app.constant("PushNoti", {
  "senderID": "381355627240"
})
